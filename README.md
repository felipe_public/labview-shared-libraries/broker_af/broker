# Broker Actor for Actor Framework

LabVIEW Core Library for implementing a broker pattern in Actor Framework messaging.

# Author
- Felipe Pinheiro Silva

## Installation

Just download this project or use SDM.
- Provider path: https://gitlab.com/felipe_public/labview-shared-libraries/broker_af

### Dependencies (from sdm-package.json)
        - "subscription-interface":"1.0.0",
        - "message-transport":"1.2.0"

## Usage

1. Includes the standard library (.lvlib) in your labview project.

2. The Broker Actor (usually the Root Actor) must inherit from this class (Broker.lvclass)\
![Broker Inheritance](/docs/snippets/parentclass.PNG)

3. The subscriber must inherit from the Message Tranport Interface.\
![Subscriber Inheritance](/docs/snippets/subscriber_from_interface.PNG)

4. The Publisher must send data to the right topic.

5. Launching Example\
![Launching Example](/docs/snippets/launch.png)


## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
BSD3

.
