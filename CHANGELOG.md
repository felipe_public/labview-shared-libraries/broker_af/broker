# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [1.1.0](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/compare/v1.0.1...v1.1.0) (2021-03-31)


### Features

* adds possibility of self subscribing ([#3](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/issues/3)) ([@felipefoz](https://gitlab.com/felipefoz)) ([add550a](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/add550a91471a91c5a8c3317331cddaeab9851ed))


### Tests

* **static:** fixes failing vi in test - reentrant issues ([1bbc24a](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/1bbc24a1edd69c1c2114d358468b85db2173f407))
* **unit:** adds unit test for self subscribe feature ([#3](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/issues/3)) ([4cf3cc9](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/4cf3cc931f29095a82b323008289f263b080e40e))

### 1.0.1 (2021-03-30)


### Bug Fixes

* update replace enqueuers to deal with array ([#1](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/issues/1)) ([0870d24](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/0870d241ea8196859d7ed651ea0017630c617220))


### Tests

* **unit:** add unit tests to deal with multiple subscriptions type ([aacca6b](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/aacca6bf46dbaddba82c2b42f4f99146847d4e5d))


### CI

* includes automatic version config file ([#2](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/issues/2)) ([8151a4c](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/8151a4c9395d3df8894c21884082a29a9b3084ad))
* refactor pipeline configuration and includes release job ([25552a8](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/25552a80f423a5d3a6fd66e986b26c286d0594a1))
* removes runner tag from release job ([9300954](https://gitlab.com/felipe_public/labview-shared-libraries/broker_af/broker/commit/930095406873b29fdc4c45c2502b955b22c32caf))

## [1.0.0] 2020-12-23
### Added
- feat: created Unit Tests for some cases
- doc: readme.md information
- ci: images and gitlab-ci configuration

#### Changed
- feat: updated to LabVIEW 2020 Interfaces
- feat: Split Map Types into Topic and Content
- fix: tests that failed in VI Analyzer

## [0.5.0] 2020-04-29
### Added
- Different Scenarios in *Launch Actors.vi* to choose the implementation strategy.
- First version of UML Class Diagram documentation.
- New Debug interface, not connected to the message forwarder.

### Changed
- New Message Transport Library
- Folders Organized.
- Some classes names changed in test actors.
- Dependencies removed: Payload and Header Types.
- VI Documentation updated.

## [0.4.0] 2020-02-28
### Added
- Debug Console.

### Changed
- Message Transport changed to dependency.

### Deprecated
- Message Transport left to backwards compatibility.

## [0.3.2] 2020-02-07
### Added
- Functions to Subscribe VIM.
- Function to unsubscribe from all.
- Update Enqueuer VI added to replace the enqueuer in runtime.

## [0.3.1] 2020-02-01
### Added
- Writer for data types into message transport. VIM file.

### Changed
- Dependencies updated.

## [0.3.0] 2020-01-07
### Added
- Nested Broker and Forward Method.

## [0.2.0] 2020-01-06
### Changed
- Updated payload path.

## [0.1.0] 2019-12-03
### Added
- Initial version.
